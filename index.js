// prettier-ignore
module.exports = {
  production: [
    "chrome >= 75",
    "edge >= 17",
    "firefox >= 68",
    "safari >= 12"
  ]
};
