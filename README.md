# `@gitlab/browserslist-config`

This project houses GitLab's [browserslist config][browserslist].

This config is supposed to reflect the machine readable equivalent of [GitLab's supported web browsers][supported-browsers].

Please note, that this project should _not_ be used without consideration, as other projects might have different requirements when it comes to browser versions.

[browserslist]: https://github.com/browserslist/browserslist
[supported-browsers]: https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers
